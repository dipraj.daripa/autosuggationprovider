
#include <bits/stdc++.h>
#include <unordered_map>
using namespace std;

struct Trie {

	bool isEndOfWord;

	/* nodes store a map to child node */
	map<char, Trie*> mp;
};

string readFileIntoString(const string& path) {
    ifstream input_file(path);
    if (!input_file.is_open()) {
        cerr << "Could not open the file - '"
             << path << "'" << endl;
        exit(EXIT_FAILURE);
    }
    return string((std::istreambuf_iterator<char>(input_file)), std::istreambuf_iterator<char>());
}

Trie* getNewTrieNode()
{
	Trie* node = new Trie;
	node->isEndOfWord = false;
	return node;
}


void insert(Trie*& root, const string& str)
{
	if (root == nullptr)
		root = getNewTrieNode();

	Trie* temp = root;
	for (int i = 0; i < str.length(); i++) {
		char x = str[i];

		/* make a new node if there is no path */
		if (temp->mp.find(x) == temp->mp.end())
			temp->mp[x] = getNewTrieNode();

		temp = temp->mp[x];
	}

	temp->isEndOfWord = true;
}

string serialize(Trie* root) {
        string result;
        if(!root)
            return result;
        result += "<";
        for(char c = 'a'; c <= 'z'; c++){
            if(root->mp.find(c) != root->mp.end()){
                result += string(1, c);
                result += serialize(root->mp[c]);
            }
        }
        result += ">";
        return result;
}

Trie* deserialize(string data) {
        if(data.empty())
            return NULL;
        Trie* root = new Trie();
        Trie* current = root;
        stack<Trie*> stk;
        for(char c: data){
            switch(c){
                case '<':
                    stk.push(current);
                    break;
               
                case '>':
                    stk.pop();
                    current->isEndOfWord = true;
                    break;
                default:
                    current = new Trie();
                    stk.top()->mp[c] = current;
            }
        }
        return root;
}

// /*function to search in trie*/
bool search(Trie* root, const string& str)
{
	/*return false if Trie is empty*/
	if (root == nullptr)
		return false;

	Trie* temp = root;
	for (int i = 0; i < str.length(); i++) {

		/* go to next node*/
		temp = temp->mp[str[i]];

		if (temp == nullptr)
			return false;
	}

	return temp->isEndOfWord;
}
void serchWord(Trie* currNode, vector<string> &ans, string word){
    
    if(currNode == nullptr){
            return;
    }
    if(currNode->isEndOfWord){
        
        ans.push_back(word);
        
    }
    map<char, Trie*> mp_1 = currNode->mp;
    
    for(auto it: mp_1){
        string temp = word+it.first;
        //cout << temp << endl;
        serchWord(mp_1[it.first], ans, temp);
    }
}

vector<string> advSerach(Trie* root, string str){
    vector<string> ans;
    
    Trie* currNode = root;
    
    for(int i=0;i<str.length();i++) {
        //cout << currNode -> mp[str[i]]<< endl;
        
        currNode  = currNode -> mp[str[i]];
        if(currNode == nullptr){
            return ans;
        }
    }
    
    serchWord(currNode, ans, str);
    
    return ans;
}

/*Driver function*/
int main()
{
	Trie* root = nullptr;


	// insert(root, "hello");
	
	// insert(root,"help");
	// insert(root,"helium");

    	string fname = "EnglishDictionary.csv";

   
	vector<vector<string>> content;
	vector<string> row;
	string line, word;
 
	fstream file (fname, ios::in);
	if(file.is_open())
	{
		while(getline(file, line))
		{
			row.clear();
 
			stringstream str(line);
 
			while(getline(str, word, ','))
				row.push_back(word);
			content.push_back(row);
		}
	}
	else{
		cout<<"Could not open the file\n";
    }

    

    map<string,int> dict;
 
	for(int i=0;i<content.size();i++)
	{
		dict[content[i][0]] = stoi(content[i][1]) + stoi(content[i][2]);
		
		
	}

    for(auto it: dict){
         insert(root, it.first);
    }

	string s = serialize(root);

    char *buf;
    int sz = s.size();

	std::ofstream out("serial trie.cgt" , ios::out | ios::binary);
    if(!out){
        cout <<"can't open" << endl;
        exit(1);
    }
    out.write(reinterpret_cast<char *> (&sz), sizeof(int));
    out.write(s.c_str(), sz);
    out.flush();
    out.close();
	
	ifstream filename("serial trie.cgt",ios::in | ios::binary);
    if(!out){
        cout <<"can't open" << endl;
        exit(1);
    }
    filename.read(reinterpret_cast<char *> (&sz), sizeof(int));
    buf = new char[sz];
    filename.read(buf, sz);

    string file_contents = "";
    file_contents.append(buf,sz);
    
    
	Trie* root2 = nullptr;
	
	root2 = deserialize(file_contents);
	//cout << search(root2,"helium");
	
	vector<string> ans1 = advSerach(root2, "abo");
	for(auto it: ans1){
	    cout << it <<" ";
	}

	return 0;
}
